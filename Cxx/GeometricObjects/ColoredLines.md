### Description
Displays two lines, each with a different color. See also [LongLine](Cxx/GeometricObjects/LongLine).
